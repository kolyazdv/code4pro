module ApplicationHelper
  def avatar_url(user)
    gravatar_id = Digest::MD5::hexdigest(user.email).downcase 
     "https://www.gravatar.com/avatar/40ee3dd520e7e3ddd41a163a369f248c.jpg?d=identicon&s=40"
  end
end
